<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ArticleController;
use App\Http\Controllers\FrontController;
use App\Http\Controllers\CommentController;
use App\Http\Controllers\LikeController;
use App\Http\Controllers\Auth\LoginController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('front.home');
});

Auth::routes();

// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/home', [App\Http\Controllers\FrontController::class, 'index'])->name('front.home');
Route::get('/article/{slug}', [App\Http\Controllers\FrontController::class, 'detail_article'])->name('front.detail_article');
Route::prefix('api')->group( function (){
    Route::prefix('article')->group( function (){
    	Route::get('/{visibility}/visibility', [ArticleController::class, 'get_by_visibility'])->name('article.get_by_visibility');
    });
	Route::get('/login/check', [LoginController::class, 'check_login'])->name('login.check');
});

Route::middleware(['auth'])->group(function () {
	Route::get('/my-article', [ArticleController::class, 'my_article'])->name('article.my_article');
    Route::prefix('api')->group( function (){
	    Route::prefix('article')->group( function (){
			// Route::get('/{visibility}/visibility', [ArticleController::class, 'get_by_visibility'])->name('article.get_by_visibility');
			Route::get('/login', [ArticleController::class, 'get_by_user_login'])->name('article.get_by_user_login');
			Route::get('/{id}/id', [ArticleController::class, 'get_by_id'])->name('article.get_by_id');
			Route::post('/', [ArticleController::class, 'create'])->name('article.create');
			Route::post('/{id}/update', [ArticleController::class, 'update'])->name('article.update');
			Route::post('/{id}/visibility', [ArticleController::class, 'update_visibility'])->name('article.update_visibility');
			Route::delete('/{id}/delete', [ArticleController::class, 'delete'])->name('article.delete');

		    Route::prefix('comment')->group( function (){
				Route::post('/', [CommentController::class, 'create'])->name('comment.create');
			Route::delete('/{id}/delete', [CommentController::class, 'delete'])->name('comment.delete');
		    });
		    Route::prefix('like')->group( function (){
				Route::post('/', [LikeController::class, 'like'])->name('like.like');
		    });
	    });

    });

});
