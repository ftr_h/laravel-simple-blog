<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'id' =>  1,
                'name' =>  'User 1',
                'email' => 'user1@user.com',
                'password' => bcrypt('password1'),
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'id' =>  2,
                'name' =>  'User 2',
                'email' => 'user2@user.com',
                'password' => bcrypt('password2'),
                'created_at' => now(),
                'updated_at' => now()
            ],
        ]);
    }
}
