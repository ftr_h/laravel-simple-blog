<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class CommentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('t_comment')->insert([
            [
                'id' => 1,
                'user_id' => 2,
                'article_id' => 1,
                'comment' => 'Lorem ipsum dolor sit amet',
                'root' => 0,
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'id' => 2,
                'user_id' => 1,
                'article_id' => 1,
                'comment' => 'Consectetur adipiscing elit',
                'root' => 1,
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'id' => 3,
                'user_id' => 2,
                'article_id' => 1,
                'comment' => 'Lorem ipsum dolor sit amet',
                'root' => 2,
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'id' => 4,
                'user_id' => 2,
                'article_id' => 1,
                'comment' => 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium',
                'root' => 0,
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'id' => 5,
                'user_id' => 2,
                'article_id' => 1,
                'comment' => 'totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo',
                'root' => 4,
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'id' => 6,
                'user_id' => 1,
                'article_id' => 1,
                'comment' => 'Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt',
                'root' => 4,
                'created_at' => now(),
                'updated_at' => now()
            ],
        ]);
    
    }
}
