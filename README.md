Step install: <br>
1. clone project laravel-simple-blog <br>
	- git clone https://gitlab.com/ftr_h/laravel-simple-blog.git <br>
2. cd laravel-simple-blog <br>
3. composer install <br>
4. create file .env dan copy code dari file .env.example, lalu sesuaikan dengan database <br>
5. lalu generate app key, <br>
	- php artisan key:generate <br>
6. lalu migrate table <br>
	- php artisan migrate <br>
7. seeding database/mengisi data-data untuk table di database untuk kebutuhan testing <br>
	- php artisan db:seed <br>
8. dan aplikasi sudah siap untuk digunakan <br>
9. untuk serve, bisa menggunakan command <br>
	- php artisan serve <br>