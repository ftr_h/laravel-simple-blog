<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Like extends Model
{
    use HasFactory;
    protected $table = 't_like';
    protected $fillable = [
        // 'id',
        'user_id',
        'article_id',
    ];

    public static function get_count_by_article_id($article_id){
        $data = static::where('article_id',$article_id)
            ->count();

        return $data;
    }

    public static function get_by_article_id_user_id($article_id,$user_id){
        $data = static::where('article_id',$article_id)
        	->where('user_id',$user_id)
            ->first();

        return $data;
    }

    public static function create_data($article_id,$user_id){
        $data = static::create([
            'article_id' => $article_id,
            'user_id' => $user_id,
        ]);

        return $data;
    }

    public static function delete_data_by_article_id_user_id($article_id,$user_id){
        $data = static::where('article_id', $article_id)
            ->where('user_id', $user_id)
            ->delete();

        return $data;
    }

    public static function delete_data($id){
        $data = static::where('id', $id)
            ->delete();

        return $data;
    }

}
