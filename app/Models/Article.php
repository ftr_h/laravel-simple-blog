<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    use HasFactory;
    protected $table = 't_article';
    protected $fillable = [
        // 'id',
        'user_id',
        'title',
        'desc',
        'cover',
        'visibility',
        'slug',
    ];

    public function user(){
        return $this->belongsTo(User::class,'user_id','id');
    }

    public static function get_all($title){
        $data = static::where('title','like','%'.$title.'%')
            ->get();

        return $data;
    }

    public static function get_by_visibility($visibility, $title){
        $data = static::where('visibility',$visibility)
            ->where('title','like','%'.$title.'%')
            ->get();

        return $data;
    }

    public static function get_by_id($id){
        $data = static::where('id',$id)
            ->first();

        return $data;
    }

    public static function get_by_slug($slug){
        $data = static::where('slug',$slug)
            ->first();

        return $data;
    }

    public static function get_by_user_id($user_id, $title){
        $data = static::where('user_id',$user_id)
        	->where('title','like','%'.$title.'%')
            ->get();

        return $data;
    }

    public static function create_data($title,$desc,$cover,$slug,$user_id){
        $data = static::create([
            'title' => $title,
            'desc' => $desc,
            'cover' => $cover,
            'slug' => $slug,
            'user_id' => $user_id,
            'visibility' => 1,
        ]);

        return $data;
    }

    public static function update_data($id,$title,$desc,$cover,$slug,$user_id){
        $data = static::where('id',$id)
            ->update([
                'title' => $title,
                'desc' => $desc,
                'cover' => $cover,
                'slug' => $slug,
                'user_id' => $user_id,
            ]);

        return $data;
    }

    public static function delete_data($id){
        $data = static::where('id', $id)
            ->delete();

        return $data;
    }

    public static function update_visibility($id,$visibility){
        $data = static::where('id',$id)
            ->update([
                'visibility' => $visibility,
            ]);

        return $data;
    }

}
