<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    use HasFactory;
    protected $table = 't_comment';
    protected $fillable = [
        // 'id',
        'user_id',
        'article_id',
        'comment',
        'root',
    ];

    public function user(){
        return $this->belongsTo(User::class,'user_id','id');
    }

    public function replies(){
        return $this->hasMany(Comment::class,'root','id')->with(['user']);
    }

    public static function get_by_article_id($article_id){
        $data = static::with(implode('.', array_fill(0, 100, 'replies')))
        	->with(['user'])
            ->where('article_id', $article_id)
            ->where('root', '=', '0')
            ->get();

        return $data;
    }

    public static function get_by_id($id){
        $data = static::with(['user'])
        	->where('id',$id)
            ->first();

        return $data;
    }

    public static function create_data($comment,$article_id,$user_id,$root){
        $data = static::create([
            'comment' => $comment,
            'article_id' => $article_id,
            'user_id' => $user_id,
            'root' => $root,
        ]);

        return $data;
    }

}
