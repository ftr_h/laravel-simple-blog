<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Article;
use App\Models\Comment;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class CommentController extends Controller
{
    public function create(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                "comment" => "required",
                "article_id" => "required",
                "root" => "required",
            ]);

            if ($validator->fails()) {
                return $this->error((object)NULL, $validator->errors()->first(), 400);
            }

            $comment = $request->get('comment');
            $article_id = $request->get('article_id');
            $root = $request->get('root');
            $user_id = Auth::user()->id;

            $data = Comment::create_data($comment,$article_id,$user_id,$root);

            if (!$data) {
                return $this->error((object)NULL, "Add new article failed", 400);
            }
            $data_response = Comment::get_by_id($data->id);
            $data_response->created_at_str = date_format($data_response->created_at, "l, d F Y H:i");
            return $this->success($data_response, 'Data added successfully', 200);
        } catch (Exception $e) {
            return $this->error((object)NULL, $e->getMessage(), 500);
        }
    }

}
