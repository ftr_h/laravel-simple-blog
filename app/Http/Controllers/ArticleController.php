<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Article;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class ArticleController extends Controller
{
    public function my_article()
    {
        $response = [];
        return view('article.my_article', $response);
    }

    public function get_all(Request $request)
    {
        try {
            $req_title = $request->get('title');
            $data = Article::get_all($req_title);
            return $this->success($data, 'Berhasil', 200);
        } catch (Exception $e) {
            return $this->error([], $e->getMessage(), 500);
        }
    }

    public function get_by_visibility($visibility, Request $request)
    {
        try {
            $req_title = $request->get('title');
            $data = Article::get_by_visibility($visibility,$req_title);
            return $this->success($data, 'Berhasil', 200);
        } catch (Exception $e) {
            return $this->error([], $e->getMessage(), 500);
        }
    }

    public function get_by_user_id(Request $request)
    {
        try {
            $req_user_id = $request->get('user_id');
            $req_title = $request->get('title');
            $data = Article::get_by_user_id($req_title);
            return $this->success($data, 'Berhasil', 200);
        } catch (Exception $e) {
            return $this->error([], $e->getMessage(), 500);
        }
    }

    public function get_by_user_login(Request $request)
    {
        try {
            $req_user_id = Auth::user()->id;
            $req_title = $request->get('title');
            $data = Article::get_by_user_id($req_user_id,$req_title);
            return $this->success($data, 'Berhasil', 200);
        } catch (Exception $e) {
            return $this->error([], $e->getMessage(), 500);
        }
    }

    public function get_by_id($id, Request $request)
    {
        try {
            $data = Article::get_by_id($id);
            if ($data == null) {
                return $this->error((object)NULL, 'Article not found', 400);
            }
            return $this->success($data, 'Success', 200);
        } catch (Exception $e) {
            return $this->error((object)NULL, $e->getMessage(), 500);
        }
    }

    public function create(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                "title" => "required|unique:t_article,title",
                "cover" => "required",
                "desc" => "required",
            ]);

            if ($validator->fails()) {
                return $this->error((object)NULL, $validator->errors()->first(), 400);
            }

            $title = $request->get('title');
            $desc = $request->get('desc');
            $cover = $request->file('cover');
            $slug = \Str::slug($request->title);
            $user_id = Auth::user()->id;
            // $visibility = 1;
            $cover_path = '';
            if($cover){
                $cover_upload = $request->cover->store('assets/img/article/' . Auth::user()->id, 'public');
                $cover_path = $cover_upload;
            }

            $data = Article::create_data($title,$desc,$cover_path,$slug,$user_id);

            if (!$data) {
                return $this->error((object)NULL, "Add new article failed", 400);
            }
            return $this->success($data, 'Data added successfully', 200);
        } catch (Exception $e) {
            return $this->error((object)NULL, $e->getMessage(), 500);
        }
    }

    public function update($id, Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                "title" => "required|unique:t_article,title,".$id,
                "desc" => "required",
            ]);

            if ($validator->fails()) {
                return $this->error((object)NULL, $validator->errors()->first(), 400);
            }

            $title = $request->get('title');
            $desc = $request->get('desc');
            $cover = $request->file('cover');
            $slug = \Str::slug($request->title);
            $user_id = Auth::user()->id;
            // $visibility = 1;
            $cover_old = $request->get('cover_old');
            $cover_path = '';

            if (Article::get_by_id($id) == null) {
                return $this->error((object)NULL, 'Article not found', 400);
            }
            if($cover){
                $cover_upload = $request->cover->store('assets/img/article/' . Auth::user()->id, 'public');
                $cover_path = $cover_upload;
            } else {
                $cover_path = $cover_old;
            }

            $data = Article::update_data($id,$title,$desc,$cover_path,$slug,$user_id);

            if (!$data) {
                return $this->error((object)NULL, "Add new article failed", 400);
            }
            return $this->success($data, 'Article updated successfully', 200);
        } catch (Exception $e) {
            return $this->error((object)NULL, $e->getMessage(), 500);
        }
    }

    public function delete($id, Request $request)
    {
        try {
            $get_article = Article::get_by_id($id);
            if ($get_article == null) {
                return $this->error((object)NULL, 'Article not found', 400);
            }
            $data = Article::delete_data($id);

            if (!$data) {
                return $this->error((object)NULL, "Delete article failed", 400);
            }

            return $this->success($get_article, 'Article deleted successfully', 200);
        } catch (Exception $e) {
            return $this->error((object)NULL, $e->getMessage(), 500);
        }
    }

    public function update_visibility($id, Request $request)
    {
        try {
            $visibility = $request->get('visibility');
            $data = Article::update_visibility($id,$visibility);

            if (!$data) {
                return $this->error((object)NULL, "Update visibility failed", 400);
            }
            return $this->success($data, 'Visibility updated successfully', 200);
        } catch (Exception $e) {
            return $this->error((object)NULL, $e->getMessage(), 500);
        }
    }

}
