<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Like;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

class LikeController extends Controller
{
    public function like(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                "article_id" => "required",
                "like" => "required",
            ]);

            if ($validator->fails()) {
                return $this->error((object)NULL, $validator->errors()->first(), 400);
            }

            $article_id = $request->get('article_id');
            $like = $request->get('like');
            $user_id = Auth::user()->id;

            $data_like_old = Like::get_by_article_id_user_id($article_id, $user_id);

            if ($like) {
            	if ($data_like_old == null) {
	            	$data = Like::create_data($article_id,$user_id);
		            if (!$data) {
		                return $this->error((object)NULL, "Like article failed", 400);
		            }
            	}
            } else {
            	if ($data_like_old != null) {
	            	$data = Like::delete_data_by_article_id_user_id($article_id,$user_id);
		            if (!$data) {
		                return $this->error((object)NULL, "Unlike article failed", 400);
		            }
            	}
            }

            $data_like_count = Like::get_count_by_article_id($article_id);
            $data_like = Like::get_by_article_id_user_id($article_id, $user_id);
            $data_response = [];
            $data_response['like'] = $data_like;
            $data_response['like_count'] = $data_like_count;
            return $this->success($data_response, 'success', 200);
        } catch (Exception $e) {
            return $this->error((object)NULL, $e->getMessage(), 500);
        }
    }

}
