<?php

namespace App\Http\Controllers;
use App\Models\Article;
use App\Models\Comment;
use App\Models\Like;
use Illuminate\Support\Facades\Auth;

use Illuminate\Http\Request;

class FrontController extends Controller
{
    public function index()
    {
        $response = [];
        return view('front.index', $response);
    }
    public function detail_article($slug)
    {
        $response = [];
        $data_article = Article::get_by_slug($slug);
        $user_id = Auth::user() == null ? null : Auth::user()->id;
        $article_id = $data_article->id;
        $data_like_count = Like::get_count_by_article_id($article_id);
        $data_like = Like::get_by_article_id_user_id($article_id, $user_id);
        $response['article'] = $data_article;
        $response['comments'] = Comment::get_by_article_id($data_article->id); 
        $response['like_count'] = $data_like_count;
        $response['like'] = $data_like;
        $response['is_login'] = Auth::user() == null ? false : true;
        return view('front.detail', $response);
    }
}
