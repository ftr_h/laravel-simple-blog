<?php if (count($comments) > 0): ?>
    <!-- <div class="card card_comment" style="margin-left: 25px;"> -->
        <?php foreach ($comments as $key => $comment): ?>
            <br>
            <div class="card">
                <div class="card-header">
                    <strong>{{ $comment->user->name }}</strong><br>
                    <small>{{date_format($comment->created_at, "l, d F Y H:i")}}</small>
                </div>
                <div class="card-body" id="card-body-{{ $comment->id }}">
                    <p>{{ $comment->comment }}</p>
                    <a href="" id="reply"></a>
                    <button class="btn btn-sm btn-outline-danger py-0" id="bt-reply-{{ $comment->id }}" style="margin-bottom: 10px" attr-comment-id="{{ $comment->id }}" onclick="reply_comment('{{ $comment->id }}')">Reply</button>
                    <div class="reply-form" id="reply-form-{{ $comment->id }}" style="display: none;">
                        <div class="row">
                            <div class="col-12">
                                <!-- <input type="text" name="comment" id="imput-reply-{{ $comment->id }}" class="form-control" cols="5" style="margin-bottom: 5px"> -->

                                <textarea name="comment" id="input-reply-{{ $comment->id }}" cols="5" class="form-control" style="margin-bottom: 5px"></textarea>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <button class="btn btn-sm btn-outline-primary py-0" attr-comment-id="{{ $comment->id }}" onclick="cancel_reply('{{ $comment->id }}')">Cancel</button>
                                <button class="btn btn-sm btn-primary py-0" attr-comment-id="{{ $comment->id }}" onclick="submit_comment('{{ $comment->id }}')">Submit</button>
                            </div>
                        </div>
                    </div>
                    <!-- <br> -->
                    @include('front.comments', ['comments' => $comment->replies, 'post_id' => $post_id])
                </div>
            </div>
        <?php endforeach ?>
    <!-- </div> -->
<?php endif ?>