@extends('layouts.app')

@section('content')
<style>
    #list_article {
        display: flex;
        flex-wrap: wrap;
        width: 100%;
    }
.col_article {
  display: flex;
  flex-direction: column;
  /*display: flex;*/
  /*margin: 10px;
  border: 1px solid #DDD;*/
  /*width: calc(20% - 20px);*/
}
    .card_article {
        height: 100%;
    }
    /*.card_article_body {
        display: flex;
    }
    .bt_article {
        justify-content: flex-end;
    }*/
</style>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <!-- <div class="card-header"><h5 style="margin-bottom: 0;"></h5></div> -->
                <div class="card-body">
                    <div class="row">
                        <div class="col-12 text-center">
                            <h3>Blog Indokoding</h3>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-8 col-sm-12"></div>
                        <div class="col-md-3 col-sm-12">
                            <input type="text" name="search" id="input_search" class="form-control" value="">
                            
                        </div>
                        <div class="col-md-1 col-sm-12 text-right">
                            <button class="btn btn-outline-primary" id="bt-search" style="float: right;" onclick="load_list_article()">Search</button>
                        </div>
                    </div>
                    <div class="row" id="list_article"></div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')
<script>

    $(document).ready(function () {
        load_list_article();
    });
    function load_list_article() {
        var formData = {};
        formData.title = $('#input_search').val();
        $.ajax({
            url: "{{url('api/article/1/visibility')}}",
            method: 'GET',
            data: formData,
            contentType: 'application/json',
            success: function (e, type) {
                var data_articles = e.data;
                var data_article_html = ``;
                for (var i = 0; i < data_articles.length; i++){
                    var html_article_visibility = '';
                    var html_article_hide_show_button = '';
                    data_article_html += `
                        <div class="col-md-3 col-sm-12 mt-3 col_article">
                            <div class="card card_article">
                                <img src="${data_articles[i].cover}" class="card-img-top" alt="gambar" height="100">
                                <div class="card-body card_article_body">
                                    <h5 class="card-title">${data_articles[i].title}</h5>
                                    <a href="/article/${data_articles[i].slug}" class="btn btn-primary bt_article">Read more</a>
                                </div>
                            </div>
                        </div>`;
                }
                $('#list_article').html(data_article_html);
            },
            error: function (e) {
                alert_message('error', e.responseJSON.message);
            }
        });
    }
</script>
@endsection