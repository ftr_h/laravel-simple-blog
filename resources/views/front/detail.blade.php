@extends('layouts.app')

@section('content')
<style>
    @media (min-width:320px)  {
        .img-cover {
            max-width: 150px;
        }}
    }
    @media (min-width:481px)  {
        .img-cover {
            max-width: 300px;
        }}
    }
    @media (min-width:641px)  { 
        .img-cover {
            max-width: 300px;
        }}
    @media (min-width:961px)  {
        .img-cover {
            max-width: 500px;
        }
    }
    @media (min-width:1025px) {
        .img-cover {
            max-width: 500px;
        }
    }
    @media (min-width:1281px) {
        .img-cover {
            max-width: 500px;
        }
    }
    .btn.love {
        box-shadow: 1px 1px 0 rgba(255,255,255,0.5) inset;
        border-radius: 3px;
        border: 1px solid;
        display: inline-block;
        height: 18px;
        line-height: 18px;
        padding: 0 8px;
        position: relative;

        font-size: 12px;
        text-decoration: none;
        text-shadow: 0 1px 0 rgba(255,255,255,0.5);
    }
/*
 * Counter button style
 */
    /*.btn-counter { margin-right: 39px; }*/
    .btn-counter:after,
    .btn-counter:hover:after { text-shadow: none; }
    /*.btn-counter:after {
      border-radius: 3px;
      border: 1px solid #d3d3d3;
      background-color: #eee;
      padding: 0 8px;
      color: #777;
      content: attr(data-count);
      left: 100%;
      margin-left: 8px;
      margin-right: -13px;
      position: absolute;
      top: -1px;
    }*/
    .btn-counter:before {
        transform: rotate(45deg);
        filter: progid:DXImageTransform.Microsoft.Matrix(M11=0.7071067811865476, M12=-0.7071067811865475, M21=0.7071067811865475, M22=0.7071067811865476, sizingMethod='auto expand');

        background-color: #eee;
        border: 1px solid #d3d3d3;
        border-right: 0;
        border-top: 0;
        content: '';
        position: absolute;
        right: -13px;
        top: 5px;
        height: 6px;
        width: 6px;
        z-index: 1;
        zoom: 1;
    }
    /*
     * Custom styles
     */
    .btn-love {
        background-color: #dbdbdb;
        border-color: #bbb;
        color: #666;
    }
    .btn-love:hover,
    .btn-love.active {
        text-shadow: 0 1px 0 #b12f27;
        background-color: #f64136;
        border-color: #b12f27;
    }
    .btn-love:active { box-shadow: 0 0 5px 3px rgba(0,0,0,0.2) inset; }
    .btn-love span { color: #f64136; }
    .btn-love:hover, .btn-love:hover span,
    .btn-love.active, .btn-love.active span { color: #eeeeee; }
    .btn-love:active span {
        color: #b12f27;
        text-shadow: 0 1px 0 rgba(255,255,255,0.3);
    }
</style>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <!-- <div class="card-header"><h5 style="margin-bottom: 0;"></h5></div> -->
                <div class="card-body">
                    <div class="row">
                        <div class="col-11">
                            <h3>{{$article->title}}</h3>
                            <hr>
                        </div>
                        <div class="col-1 text-center">
                            <a href="#" title="Love it" id="bt-love" class="btn btn-love btn-counter {{($like == null) ? '' : 'active'}}" data-count="0"><span>&#x2764;</span></a><br>
                            <label id="counter_like">{{$like_count}}</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <label>{{date_format($article->created_at, "l, d F Y H:i")}}</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <label>by: {{$article->user->name}}</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 text-center">
                            <img src="{{url($article->cover)}}" class="img-fluid img-cover" alt="gambar">
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-12">
                            <p>{{$article->desc}}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <div class="col-md-12">
            <div class="card">
                <!-- <div class="card-header"><h5 style="margin-bottom: 0;"></h5></div> -->
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <h5>Comments</h5>
                            <hr>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12" id="card-body-0">
                            @include('front.comments', ['comments' => $comments, 'post_id' => $article->id])
                        </div>
                    </div>

                    <h5>Leave a comment</h5>
                    <div class="form-group">
                        <textarea name="comment" id="input-reply-0" cols="5" class="form-control" required></textarea>
                        <!-- <input type="text" name="comment" class="form-control" cols="5"> -->
                        <input type="hidden" name="article_id" id="article_id" value="{{$article->id}}">
                    </div>
                    <div class="form-group">
                        <button class="btn btn-sm btn-primary py-0" attr-comment-id="0" onclick="submit_comment('0')">Add Comment</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')
<script>

    $(document).ready(function () {
    });
    function reply_comment(comment_id) {
        $('#bt-reply-'+comment_id).hide();
        $('#reply-form-'+comment_id).show();
    }
    function cancel_reply(comment_id) {
        $('#input-reply-'+comment_id).val('');
        $('#bt-reply-'+comment_id).show();
        $('#reply-form-'+comment_id).hide();
    }

    function submit_comment(comment_id) {
        if (!check_login()) {
            alert('Login required to access this function');
            return;
        }
        var proceed = confirm("Are you sure to submit this comment?");
        if (proceed) {
            var formData = {};
            formData.root = comment_id;
            formData.article_id = $('#article_id').val();
            formData.comment = $('#input-reply-'+comment_id).val();
            $.ajax({
                type: 'POST',
                url: "{{route('comment.create')}}",
                data: JSON.stringify(formData),
                cache: false,
                contentType: false,
                processData: false,
                contentType: 'application/json',headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: (data) => {
                    if (data.code == 200) {
                        var data_comment = data.data;
                        $('#input-reply-'+comment_id).val('');
                        var html_comment = `<br>
                            <div class="card">
                                <div class="card-header">
                                    <strong>${data_comment.user.name}</strong><br>
                                    <small>${data_comment.created_at_str}</small>
                                </div>
                                <div class="card-body" id="card-body-${data_comment.id}">
                                    <p>${data_comment.comment}</p>
                                    <a href="" id="reply"></a>
                                    <button class="btn btn-sm btn-outline-danger py-0" id="bt-reply-${data_comment.id}" style="margin-bottom: 10px" attr-comment-id="${data_comment.id}" onclick="reply_comment('${data_comment.id}')">Reply</button>
                                    <div class="reply-form" id="reply-form-${data_comment.id}" style="display: none;">
                                        <div class="row">
                                            <div class="col-12">
                                                <!-- <input type="text" name="comment" id="imput-reply-${data_comment.id}" class="form-control" cols="5" style="margin-bottom: 5px"> -->

                                                <textarea name="comment" id="input-reply-${data_comment.id}" cols="5" class="form-control" style="margin-bottom: 5px"></textarea>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-12">
                                                <button class="btn btn-sm btn-outline-primary py-0" attr-comment-id="${data_comment.id}" onclick="cancel_reply('${data_comment.id}')">Cancel</button>
                                                <button class="btn btn-sm btn-primary py-0" attr-comment-id="${data_comment.id}" onclick="submit_comment('${data_comment.id}')">Submit</button>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- <br> -->
                                </div>
                            </div>`;
                        $('#card-body-'+comment_id).append(html_comment);
                        if (comment_id != 0) {
                            cancel_reply(comment_id);
                        }
                        alert_message('success', data.message);
                    } else {
                        alert_message('error', data.message);
                    }
                },
                error: function(data){
                    alert_message('error', data.responseJSON.message);
                }
            });
        }
    }

    $('#bt-love').on('click', function(event, count) {
        event.preventDefault();
        
        if (!check_login()) {
            alert('Login required to access this function');
            return;
        }
        var confirm_message = '';
        var data_like = true;
        if ($('#bt-love').hasClass('active')) {
            confirm_message = 'Are you sure to unlike this article?';
            data_like = false;
        } else {
            confirm_message = 'Like this article?';
            data_like = true;
        }
        var proceed = confirm(confirm_message);
        if (proceed) {
            console.log(proceed);
            var formData = {};
            formData.article_id = $('#article_id').val();
            formData.like = data_like;
            $.ajax({
                type: 'POST',
                url: "{{route('like.like')}}",
                data: JSON.stringify(formData),
                cache: false,
                contentType: false,
                processData: false,
                contentType: 'application/json',headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: (data) => {
                    if (data.code == 200) {
                        if (data.data.like != null) {
                            $('#bt-love').addClass('active');
                        } else {
                            $('#bt-love').removeClass('active');
                        }
                        $('#counter_like').html(data.data.like_count);
                        alert_message('success', data.message);
                    } else {
                        alert_message('error', data.message);
                    }
                },
                error: function(data){
                    alert_message('error', data.responseJSON.message);
                }
            });
        }
    });
    function check_login() {
        var is_login = "{{$is_login}}";
        if (is_login) {
            return true;
        } else {
            return false;
        }
    }
</script>
@endsection