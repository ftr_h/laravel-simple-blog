@extends('layouts.app')

@section('content')
<style>
    .bt-action {
        margin-right: 2.5px;
        margin-left: 2.5px;
        /*min-width: 70px;*/
    }
    #header-form {
        font-size: 18px;
        font-weight: bold;
    }
    .clearfix::after { 
        content: " ";
        display: block; 
        height: 0; 
        clear: both;
    }
</style>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header"><h5 style="margin-bottom: 0;">My Article</h5></div>
                <ul class="nav nav-tabs" id="tab-manage-" role="tablist" hidden="true">
                    <li class="nav-item" role="presentation">
                        <button class="nav-link active" id="table-content-tab" data-bs-toggle="tab" data-bs-target="#table-content" type="button" role="tab" aria-controls="table-content" aria-selected="true">table-content</button>
                    </li>
                    <li class="nav-item" role="presentation">
                        <button class="nav-link" id="form-content-tab" data-bs-toggle="tab" data-bs-target="#form-content" type="button" role="tab" aria-controls="form-content" aria-selected="false">form-content</button>
                    </li>
                </ul>
                <div class="card-body">

                    <div class="alert alert-danger alert-dismissible fade show clearfix" id="alert_error" role="alert" style="display: none;">
                        <label id="alert_error_label" style="float: left; margin-top: 7.5px;margin-bottom: 7.5px;">
                            Error
                        </label>
                      
                        <button type="button" class="btn btn-primary-outline close" data-dismiss="alert" aria-label="Close" style="float: right;" onclick="$(this).parent().hide()">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="alert alert-success alert-dismissible fade show clearfix" id="alert_success" role="alert" style="display: none;">
                        <label id="alert_success_label" style="float: left; margin-top: 7.5px;margin-bottom: 7.5px;">
                            Success
                        </label>
                      
                        <button type="button" class="btn btn-primary-outline close" data-dismiss="alert" aria-label="Close" style="float: right;" onclick="$(this).parent().hide()">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="tab-content" id="tab-manage-content">
                        <div class="tab-pane fade show active" id="table-content" role="tabpanel" aria-labelledby="table-content-tab">
                            <div class="row">
                                <div class="col-12">
                                    <button class="btn btn-success my-3" onclick="add_article()">Add Article</button>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-8 col-sm-12"></div>
                                <div class="col-md-3 col-sm-12">
                                    <input type="text" name="search" id="input_search" class="form-control" value="">
                                    
                                </div>
                                <div class="col-md-1 col-sm-12 text-right">
                                    <button class="btn btn-outline-primary" id="bt-search" style="float: right;" onclick="load_data_table()">Search</button>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">

                                    <div class="table-responsive">
                                        <div class="row">
                                            <div class="col-12">
                                                <table class="table">
                                                    <thead>
                                                        <tr>
                                                            <th scope="col" width="5%">#</th>
                                                            <th scope="col" width="20%">Title</th>
                                                            <th scope="col" width="40%">Description</th>
                                                            <th scope="col" width="10%">Visibility</th>
                                                            <th scope="col" width="20%">Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody id="tb-article-body">
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="form-content" role="tabpanel" aria-labelledby="form-content-tab">
                            <div class="row">
                                <div class="col-12">
                                    <button class="btn btn-success" id="bt-back-article">< Back</button>
                                    <label id="header-form">Add new article</label>
                                </div>
                            </div>
                            <br>
                            <form id="form_article">
                                @csrf
                                <div class="row">
                                    <div class="col-md-9">
                                        <div class="form-group">
                                            <label for="title">Title</label>
                                            <input type="text" name="title" id="form_input_title" class="form-control" value="" required>
                                            <input type="hidden" name="id" id="form_input_id">
                                        </div>
                                        <div class="form-group">
                                            <label for="desc">Description</label>
                                            <textarea name="desc" id="form_input_desc" cols="30" rows="10" class="form-control" required></textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="row">
                                            <div class="col-12">
                                                <label for="cover">Cover</label>
                                                <img id="img_cover_output" src="{{url('assets/img/dummy/dummy-image-square.jpg')}}" width="100%">
                                                <input type="file" name="cover" class="form-control" value="" id="form_input_cover">
                                                <input type="hidden" name="cover_old" id="form_input_cover_old">
                                            </div>
                                        </div>
                                        <br>
                                        <div class="row">
                                            <div class="col-12">
                                                <button type="submit" class="btn btn-primary" id="bt-save-article" style="width:100%">POST</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script>
    var form_action_type = '';
    $(document).ready(function () {
        load_data_table();
    });
    function load_data_table() {
        var postData = {};
        postData.title = $('#input_search').val();
        $.ajax({
            url: "{{route('article.get_by_user_login')}}",
            method: 'GET',
            data: postData,
            contentType: 'application/json',
            success: function (e, type) {
                var data_articles = e.data;
                var data_article_html = ``;
                for (var i = 0; i < data_articles.length; i++){
                    var html_article_visibility = '';
                    var html_article_hide_show_button = '';
                    var html_article_desc = (data_articles[i].desc.length > 100) ? data_articles[i].desc.substr(0,100) + '...' : data_articles[i].desc;
                    if (data_articles[i].visibility == 1) {
                        html_article_visibility = `<button class="btn btn-outline-success">Visible</button>`;
                        html_article_hide_show_button = `<button class="btn btn-warning bt-action" onclick="update_visibility(${data_articles[i].id},0)">Hide</button>`;
                    } else {
                        html_article_visibility = `<button class="btn btn-outline-danger">Hidden</button>`;
                        html_article_hide_show_button = `<button class="btn btn-warning bt-action" onclick="update_visibility(${data_articles[i].id},1)">Show</button>`;
                    }
                    data_article_html += `
                        <tr>
                            <td>${i+1}</td>
                            <td>${data_articles[i].title}</td>
                            <td> 
                                ${html_article_desc}
                            </td>
                            <td> 
                                ${html_article_visibility}
                            </td>
                            <td>
                                <button class="btn btn-primary bt-action" onclick="edit_article(${data_articles[i].id})">Edit</button>
                                ${html_article_hide_show_button}
                                <button class="btn btn-danger bt-action" onclick="delete_article(${data_articles[i].id})">Delete</button>
                            </td>
                        </tr>`;
                }
                $('#tb-article-body').html(data_article_html);
            },
            error: function (e) {
                alert_message('error', data.responseJSON.message);
            }
        });
    }
    function add_article() {
        $('#header-form').html('Add new article');
        reset_form();
        form_action_type = 'add';
        $('#form-content-tab').click();
    }
    function edit_article(id) {
        $('#header-form').html('Edit article');
        reset_form();
        form_action_type = 'edit';
        $.ajax({
            type:'GET',
            url: 'api/article/'+id+'/id',
            contentType: 'application/json',
            success: (data) => {
                var data_article = data.data;
                $('#form_input_title').val(data_article.title);
                $('#form_input_id').val(data_article.id);
                $('#form_input_desc').val(data_article.desc);
                $('#form_input_cover_old').val(data_article.cover);
                $('#img_cover_output').attr('src', data_article.cover);
                $('#form-content-tab').click();
            },
            error: function(data){
                alert_message('error', data.responseJSON.message);
            }
        });
    }
    function delete_article(id) {
        var proceed = confirm("Are you sure to permanently delete this data?");
        if (proceed) {
            $.ajax({
                type:'DELETE',
                url: 'api/article/'+id+'/delete',
                contentType: 'application/json',headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: (data) => {
                    if (data.code == 200) {
                        load_data_table();
                        alert_message('success', data.message);
                    } else {
                        alert_message('error', data.message);
                    }
                },
                error: function(data){
                    alert_message('error', data.responseJSON.message);
                }
            });
        }
    }
    function update_visibility(id,visibility) {
        var message_confirmation = (visibility == 1) ? "Are you sure to set visible this article?" : "Are you sure to set hidden this article?";
        var proceed = confirm(message_confirmation);
        if (proceed) {
            var formData = {};
            formData.visibility = visibility;
            $.ajax({
                type: 'POST',
                url: 'api/article/'+id+'/visibility',
                data: JSON.stringify(formData),
                cache: false,
                contentType: false,
                processData: false,
                contentType: 'application/json',headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: (data) => {
                    if (data.code == 200) {
                        load_data_table();
                        alert_message('success', data.message);
                    } else {
                        alert_message('error', data.message);
                    }
                },
                error: function(data){
                    alert_message('error', data.responseJSON.message);
                }
            });
        }
    }
    function reset_form() {
        form_action_type = '';
        $('#form_input_title').val('');
        $('#form_input_id').val('');
        $('#form_input_desc').val('');
        $('#form_input_cover').val('');
        $('#form_input_cover_old').val('');
        $('#img_cover_output').attr('src', "{{url('assets/img/dummy/dummy-image-square.jpg')}}");
    }
    $("#form_input_cover").change(function() {
        if ($(this).val() == '') {
            $('#img_cover_output').attr('src', "{{url('assets/img/dummy/dummy-image-square.jpg')}}");
        } else {
            $('#img_cover_output').attr('src', window.URL.createObjectURL(this.files[0]));
        }
    });

    // $( "#bt-save-article" ).click(function() {

    //     var title = $('#form_title').val();
    //     var desc = $('#form_desc').val();
    //     var getTypeTab = $('.nav-form-type.active').attr('attr-value');
    //     var model = {};
    //     model.shopCode = ShopCode;
    //     $.ajax({
    //         url: "Line/GetLineByShopCode",
    //         method: 'POST',
    //         data: JSON.stringify(model),
    //         contentType: 'application/json',
    //         success: function (e, type) {
    //             console.log(e);
    //         },
    //         error: function (e) {
    //             //toastr.error(e.Message);
    //         }

    //     });
    //     // alert( "SAVE" );
    // });
    $( "#bt-back-article" ).click(function() {
        $('#table-content-tab').click();
    });
    $('#form_article').submit(function(e) {
        e.preventDefault();
        if (form_action_type == 'add' || form_action_type == 'edit') {
            var route_url = '';
            var route_method = '';
            var id = $('#form_input_id').val();
            var formData = new FormData(this);
            if (form_action_type == 'add') {
                route_method = 'POST';
                route_url = "{{route('article.create')}}";
                if ($('#form_input_cover').val() == '') {
                    alert_message('error', 'Cover is required');
                    return;
                }
            } else if (form_action_type == 'edit') {
                route_method = 'POST';
                route_url = "{{url('')}}"+"/api/article/"+id+"/update";
                if ($('#form_input_cover').val() == '' && $('#form_input_cover').attr('src') == "{{url('assets/img/dummy/dummy-image-square.jpg')}}") {
                    alert_message('error', 'Cover is required');
                    return;
                }
            }
            $.ajax({
                type: route_method,
                url: route_url,
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                success: (data) => {
                    // console.log(data);
                    if (data.code == 200) {
                        $('#input_search').val('');
                        load_data_table();
                        $('#table-content-tab').click();
                        alert_message('success', data.message);
                    } else {
                        alert_message('error', data.message);
                    }
                },
                error: function(data){
                    alert_message('error', data.responseJSON.message);
                }
            });
        } else {

        }
        
    });
</script>
@endsection